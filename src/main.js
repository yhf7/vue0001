/*
 * @Author: yhf 
 * @Date: 2018-10-21 10:04:42 
 * @Last Modified by: yhf
 * @Last Modified time: 2018-11-03 22:55:53
 */

//  main.js 入口文件


// 导入 vue
import Vue from 'vue';


// 1.1 导入路由包
import VueRouter from 'vue-router';
// 1.2 安装路由
Vue.use(VueRouter);


// 注册vuex
import Vuex from 'vuex';
Vue.use(Vuex);

let car = JSON.parse(localStorage.getItem('car') || '[]')

let store = new Vuex.Store({
    state: { // this.$store.state.***
        car: car // 将购物车中的数据用数组存储
    },
    mutations: { // this.$store.commit('方法名','按需传递唯一参数')
        addToCar(state, goodsinfo) {
            // 点击添加购物车，把商品信息，保存到 store 中到 car
            // 假设 在购物车中，没有找到对应到商品
            let flag = false;
            state.car.some(item => {
                if (item.id == goodsinfo.id) {
                    item.count += parseInt(goodsinfo.count);
                    flag = true;
                    return true
                }
            })

            // 如果最终，循环完毕，得到 flag 还要 false， 则把商品数据直接 push 到购物车中
            if (!flag) {
                state.car.push(goodsinfo)
            }

            // 当 更新 car 之后，把 car数组，存储到 本地存储
            localStorage.setItem('car', JSON.stringify(state.car))
        },
        updateGoodsInfo(state, goodsinfo) {
            // 修改购物车中的商品值
            // 分析：
            state.car.some(item => {
                if (item.id == goodsinfo.id) {
                    item.count = parseInt(goodsinfo.count);
                    return true;
                }
            })
            // 当 更新 car 之后，把 car数组，存储到 本地存储
            localStorage.setItem('car', JSON.stringify(state.car))
        },
        removeFormCar(state, id) {
            // 根据id 从 store 中的购物车中删除对应的那条商品数据
            state.car.some((item, i) => {
                if (item.id == id) {
                    state.car.splice(i, 1);
                    return true;
                }
            })
            // 当 更新 car 之后，把 car数组，存储到 本地存储
            localStorage.setItem('car', JSON.stringify(state.car))
        },
        updateGoodsSelected(state, info) {
            state.car.some(item => {
                if (item.id == info.id) {
                    item.selected = info.selected
                }
            })
            // 当 更新 car 之后，把 car数组，存储到 本地存储
            localStorage.setItem('car', JSON.stringify(state.car))
        }
    },
    getters: { // this.$store.getters.***
        getAllCount(state) {
            let c = 0;
            state.car.forEach(item => {
                c += item.count;
            })
            return c
        },
        getGoodsCount(state) {
            var o = [];
            state.car.forEach(item => {
                o[item.id] = item.count;
            })
            return o;
        },
        getGoodsSelected(state) {
            var o = {};
            state.car.forEach(item => {
                o[item.id] = item.selected;
            })
            return o;
        },
        getGoodsCountAndAmount(state) {
            var o = {
                count: 0, // 勾选的数量
                amount: 0 // 勾选的总价
            };
            state.car.forEach(item => {
                if (item.selected) {
                    o.count += item.count;
                    o.amount += item.price * item.count
                }
            })
            return o
        }
    }
})


// 导入时间插件
import moment from 'moment';
// 定义全局的过滤器
Vue.filter('dateFormat', function (dataStr, pattern = "YYYY-MM-DD HH:mm:ss") {
    return moment(dataStr).format(pattern)
})

import VueResource from 'vue-resource';
Vue.use(VueResource);
// 设置请求的根路径
Vue.http.options.root = 'http://127.0.0.1:5000';
// 全局设置 post 时候表单数据格式组织形式   application/x-www-form-urlencoded
Vue.http.options.emulateJSON = true;


// 导入 Mui 样式
import './lib/mui/css/mui.min.css';
// 导入 Mui 的拓展样式
import './lib/mui/css/icons-extra.css';

// 按需导入 Mint-UI 中的组件
// import {
//     Header,
//     Swipe,
//     SwipeItem,
//     Button,
//     Lazyload
// } from 'mint-ui';
// Vue.component(Button.name, Button);
// Vue.component(Header.name, Header);
// Vue.component(Swipe.name, Swipe);
// Vue.component(SwipeItem.name, SwipeItem);
// Vue.use(Lazyload);

// 全部导入 mint-ui 
import MintUI from 'mint-ui';
Vue.use(MintUI);
// 导入样式
import 'mint-ui/lib/style.css';

// 安装图片预览插件
import VuePreview from 'vue2-preview'
Vue.use(VuePreview)



// 1.3 导入自己的 router.js 路由模块
import router from './router.js'

// 导入 app 根组件
import app from './App.vue'

let vm = new Vue({
    el: '#app',
    render: c => c(app),
    router, // 1.4 挂载路由对象到 vm 实例子
    store // 挂在store状态管理
})